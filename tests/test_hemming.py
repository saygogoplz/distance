import pytest

from distance.text.edit import hamming_distance


def test_hamming_distance():
    assert hamming_distance("abcd", "abcd") == 0
