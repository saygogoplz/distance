import pytest

from distance.text.edit import levenshtein_distance


def test_levenshtein_distance():
    assert levenshtein_distance("abcd", "abcd") == 0
