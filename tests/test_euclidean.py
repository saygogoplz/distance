import pytest

from distance.metric import euclidean_distance


def test_euclidean_distance():
    assert euclidean_distance([0, 0, 0], [0, 0, 0]) == 0
    assert euclidean_distance([3, 0, 0], [0, 4, 0]) == 5
