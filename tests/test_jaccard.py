import pytest

from distance.text.token import jaccard_distance


def test_hamming_distance():
    assert jaccard_distance("abcd", "abcd") == 0
