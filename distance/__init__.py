"""Distance package"""
from distance import text
from distance import metric

__all__ = ["text", "metric"]
