"""Levenshtein token distance implementation"""
from typing import AnyStr

from textdistance import levenshtein  # type: ignore


def levenshtein_distance(lhs: AnyStr, rhs: AnyStr) -> float:
    """
    Calculates Hamming Distance of two strings.

    >>> levenshtein_distance("abcd", "abcd")
    0
    """
    return levenshtein(lhs, rhs)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
