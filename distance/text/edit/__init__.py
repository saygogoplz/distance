"""Distance.text.edit submodule"""
from ._hemming import hamming_distance, hammingDistance
from ._levenshtein import levenshtein_distance

__all__ = ["hamming_distance", "levenshtein_distance", "hammingDistance"]
