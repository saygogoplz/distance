"""Hemming edit distance implementation"""
from typing import AnyStr

from textdistance import hamming  # type: ignore
from ..._util import deprecated


@deprecated(
    version='0.0.1',
    reason="You should use snake_case form"
)  # pylint: disable=C0103
def hammingDistance(lhs: AnyStr,
                    rhs: AnyStr) -> float:
    """
    Calculates Hamming Distance of two strings.

    >>> hamming_distance("abcd", "abcd")
    0
    """
    return hamming(lhs, rhs)


def hamming_distance(lhs: AnyStr, rhs: AnyStr) -> float:
    """
    Calculates Hamming Distance of two strings.

    >>> hamming_distance("abcd", "abcd")
    0
    """
    return hamming(lhs, rhs)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
