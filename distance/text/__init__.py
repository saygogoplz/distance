"""Distance.text submodule"""
from distance.text import token
from distance.text import edit


__all__ = ["edit", "token"]
