"""Distance.text.token submodule"""
from ._jaccard import jaccard_distance

__all__ = ["jaccard_distance"]
