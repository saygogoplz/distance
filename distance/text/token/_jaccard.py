"""Jaccard token distance implementation"""
from typing import AnyStr


def jaccard_distance(lhs: AnyStr, rhs: AnyStr) -> float:
    """
    Calculates Jaccard Distance of two strings.

    >>> jaccard_distance("abcd", "abcd")
    0
    """
    intersection = len(list(set(rhs).intersection(lhs)))
    union = (len(lhs) + len(rhs)) - intersection
    return 1 - float(intersection) / union


if __name__ == "__main__":
    import doctest
    doctest.testmod()
