"""Distance.metric submodule"""
from ._euclidean import euclidean_distance, euclideanDistance

__all__ = ["euclidean_distance", "euclideanDistance"]
