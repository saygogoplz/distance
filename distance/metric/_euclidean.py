"""Euclidean metric distance implementation"""
from typing import Collection

import math
from .._util import deprecated


@deprecated(
    version='0.0.1',
    reason="You should use snake_case form"
)   # pylint: disable=C0103
def euclideanDistance(lhs: Collection[float],
                      rhs: Collection[float]) -> float:
    """
    Calculates Euclidean Distance of two length equal iterables.

    >>> euclideanDistance([0, 0, 0], [0, 0, 0])
    0
    >>> euclideanDistance([3, 0, 0], [0, 4, 0])
    5
    """
    assert len(lhs) == len(rhs)
    mean = sum(
        (left - right) ** 2 for left, right in zip(lhs, rhs)
    )
    return math.sqrt(mean)


def euclidean_distance(lhs: Collection[float],
                       rhs: Collection[float]) -> float:
    """
    Calculates Euclidean Distance of two length equal iterables.

    >>> euclidean_distance([0, 0, 0], [0, 0, 0])
    0
    >>> euclidean_distance([3, 0, 0], [0, 4, 0])
    5
    """
    assert len(lhs) == len(rhs)
    mean = sum(
        (left - right) ** 2 for left, right in zip(lhs, rhs)
    )
    return math.sqrt(mean)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
